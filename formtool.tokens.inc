<?php


function formtool_token_info()
{
  $token_defaults = array(
    'description' => ' ',
    'dynamic' => true,
    'type' => 'form'
  );

  $info['tokens']['entityform']['value'] = array(
    'name' => 'Field value',
    'description' => ' ',
    'entity-token' => true,
    'dynamic' => true
  );

  $info['types']['form']['name'] = 'Form tokens';
  $info['types']['form']['description'] = ' ';
  $info['types']['form']['needs-data'] = 'form';

  $info['tokens']['form']['element']        = array('name' => 'Element') + $token_defaults;
  $info['tokens']['form']['input']          = array('name' => 'Input element') + $token_defaults;
  $info['tokens']['form']['label']          = array('name' => 'Label element') + $token_defaults;
  $info['tokens']['form']['label-text']     = array('name' => 'Label text') + $token_defaults;
  $info['tokens']['form']['label-text-raw'] = array('name' => 'Label text') + $token_defaults;
  $info['tokens']['form']['value']          = array('name' => 'Value') + $token_defaults;
  $info['tokens']['form']['value-raw']      = array('name' => 'Raw value') + $token_defaults;
  $info['tokens']['form']['field-value']    = array('name' => 'Field value') + $token_defaults;

  return $info;
}


function formtool_tokens($type, $tokens, array $data = array(), array $options = array())
{
  $replacements = array();

  if($type === 'entityform' && !empty($data['entityform']))
  {
    foreach($tokens as $key => $original)
    {
      $parts = explode(':', $key);
      if(count($parts) !== 2)
        continue;
      switch($parts[1])
      {
        case 'value':
          $field = $parts[0];
          $wrapper = entity_metadata_wrapper($type, $data[$type]);
          $replacements[$original] = $wrapper->{$field}->value(array('identifier' => true));
      }
    }
    return $replacements;
  }

  if($type !== 'form' || !isset($data['form']))
    return array();

  $form = $data['form'];

  foreach($tokens as $key => $original)
  {
    list($type, $token) = explode(':', $key, 2) + array(null, false);
    $element = $form;
    while(strlen($token) && $element !== false)
    {
      list($name, $token) = explode(':', $token, 2) + array(null, false);
      $element = isset($element[$name]) ? $element[$name] : false;
    }
    if($element === false)
      continue;
    switch($type)
    {
      case 'element':
        $replacements[$original] = render($element);
        break;

      case 'input':
        $elements = formtool_search_all($element, '#title');
        foreach($elements as &$element)
          $element['#title_display'] = 'none';
        $element['#theme_wrappers'] = array();
        $replacements[$original] = render($element);
        break;

      case 'label':
        $element = formtool_search($element, '#title');
        $replacements[$original] = theme('form_element_label', array('element' => $element));
        break;

      case 'label-text':
      case 'label-text-raw':
        $title = isset($element['#title']) ? $element['#title'] : '';
        $replacements[$original] = $type === 'label-text' ? check_plain($title) : $title;
        break;

      case 'value':
      case 'value-raw':
        break;

      case 'field-value':
        break;

    }
  }
  return $replacements;

}